package edu.bsuir.test.user;

import edu.bsuir.pojo.User;
import edu.bsuir.test.BasicTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.rmi.RemoteException;

import static org.assertj.core.api.Assertions.assertThat;

public class UserTest extends BasicTest {
    private User getUser;
    @Before
    public void setUp() {getUser = getDummyUser();}

    @Test
    public void shouldGetUser() {
        String locationHeader = getUser ("user/", getDummyUser);
        User getUser = getResource(locationHeader, User.class);
        assertThat(getUser).isEqualToIgnoringGivenFields(getUser, "id");
    }
    @After
        private User getDummyUser(){
        return new User();
        }

    private User OneUser;

    @Test
    public void shouldGetOneUser() {
        String locationHeader = getOneUser("user/3", getUser);
        User getOneUser = getResource(locationHeader, User.class);
        assertThat(getOneUser).isEqualToIgnoringGivenFields(getOneUser, "id");
    }

    private User DeleteAllUser;

    @Test
    public void shouldDeleteUser() {
        String userService = deleteAllUser("user", deleteUser);
        when().delete("http://localhost:8080/SpringBootRestApi/api")
                .then().deleteAllUser()
                .and()
                .assertThat()
                .get("user/");
            }
}


